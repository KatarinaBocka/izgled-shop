// Lib imports
import Vue from 'vue'
import VueRouter from 'vue-router'
//import store from '@/store'
// Routes
import paths from './paths';

function route ({ path, view, name, meta }) {
    return {
        name: name || view,
        path,
        meta,
        component: async (resolve) => import(`@/views/${view}.vue`)
    }
}
const isRefreshed = (from) => !from.name

Vue.use(VueRouter);

const router = new VueRouter ({
    mode: 'history',
    routes: paths.map(path => {
        return route(path)
    })
});

router.beforeEach(async (to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // this route requires auth, check if logged in
      // if not, redirect to login page.
      if (isRefreshed(from)) {
        try {
          await store.dispatch('auth/fetch_user')
        } catch (e) {}
      }
      if (!store.getters['auth/isLoggedIn']()) {
        next({
          path: '/login',
          query: { redirect: to.fullPath }
        })
      } else {
        next()
      }
    } else {
      if (store.getters['auth/isLoggedIn']()) {
        return
      }
  
      next()
    }
  })

export default router
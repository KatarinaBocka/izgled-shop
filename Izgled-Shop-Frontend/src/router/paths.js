export default[
    { 
        path: '/', 
        view: 'Home',
        name: 'Home'
    },
    { 
        path: '/contact', 
        view: 'Contact',
        name: 'Contact'
    },
    {
        path: '/shop',
        view: 'Shop',
        name: 'Shop',
    },
    {
        path: '/about-us',
        view: 'AboutUs',
        name: 'AboutUs'
    },
    {
        path: '/login',
        view: 'Login',
        name: 'Login'
    }, 
    {
        path: '/register',
        view: 'Register',
        name: 'Register'
    }
    

] 

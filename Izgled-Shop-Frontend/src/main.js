import '@babel/polyfill'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from '@/router'
//import store from '@/store'
// Sync router with store
import { sync } from 'vuex-router-sync'

// Sync store with router
//sync(store, router)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  //store
}).$mount('#app')

import Axios from '@/plugins/axios'

const METHODS = {
  GET: 'get',
  POST: 'post',
  PUT: 'put',
  DELETE: 'delete'
}

export default class BaseApiService {
  get (url, options) {
    return this.fetch({ method: METHODS.GET, url, ...options })
  }

  post (url, data, options) {
    return this.fetch({ method: METHODS.POST, url, data, ...options })
  }

  put (url, data, options) {
    return this.fetch({ method: METHODS.PUT, url, data, ...options })
  }

  delete (url, options) {
    return this.fetch({ method: METHODS.DELETE, url, ...options })
  }

  async fetch (axiosFields) {
    try {
      const res = await Axios(axiosFields)
      return res.data ? res.data : res
    } catch (e) {
      //console.error(e)
      throw e
    }
  }

  setHostname (hostname) {
    Axios.defaults.baseURL =
      location.protocol + '//' +
      hostname + ':' + location.port +
      Axios.defaults.baseURL.replace(new RegExp(`${location.protocol}//[^/]+/`), '/')
  }
}
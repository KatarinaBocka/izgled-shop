# shop

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

vueShop
|__ README.md
|__ package.json
|__ package-lock.json
|__ babel.config.js
|__ .gitignore
|__ node_modules
|__ public
|__ src
|   |__ assets
|   |   |__ cards
|   |
|   |__ components
|   |   |__ HomeComponents
|   |   |   |__ Blog.vue
|   |   |   |__ Carousel.vue
|   |   |   |__ Collection.vue
|   |   |   |__ FeaturedProducts.vue
|   |   |   |__ Modal.vue
|   |   |   |__ QuickViewProductModal.vue
|   |   |   |__ QuickViewProductModalTab.vue
|   |   |   |__ QuickViewProductModalTabs.vue
|   |   |   |__ TabContentComponent.vue
|   |   |
|   |   |__ Shared
|   |   |   |__ GoogleMap.vue
|   |   |   
|   |   |__ ShopComponents
|   |   |   |__ AllProducts.vue
|   |   |   |__ ShopSidebar.vue
|   |   |   |__ SingleProduct.vue



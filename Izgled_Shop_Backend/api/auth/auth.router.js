const Router = require('express').Router();
const { register, login, loggedUser } = require('./auth.controller');
const { authProtectMiddleware } = require('../../middleware/auth');

Router.post('/register', register);
Router.post('/login', login);
Router.get('/loggedUser', authProtectMiddleware, loggedUser);

module.exports = Router;
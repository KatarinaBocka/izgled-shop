const User = require('../../database/models/User');
const service = require('./auth.services');
const asyncHandler = require('../../middleware/async');

/**
 * @description Register user
 * @route       POST /api/auth/register
 * @access      Public
 */
const register = asyncHandler(async (req, res, next) => {
    const { name, email, password, role } = req.body;

    // Create user
    const user = await User.create({
        name,
        email,
        password,
        role
    });

    sendTokenResponse(user, 200, res);
});

/**
 * @description Login user
 * @route       POST /api/auth/login
 * @access      Public
 */
const login = asyncHandler(async (req, res, next) => {
    const { email, password } = req.body;

    // Validate email & password
    if (!email || !password) {
        return next(new ErrorResponse('Please provide an email and password', 400));
    }

    // Check for user
    const user = await User.findOne({ email }).select('+password');

    if(!user) {
        return next(new ErrorResponse('Invalid credentials', 401));
    }

    // Check if password matches
    const isMatch = await user.matchPassword(password);

    if (!isMatch) {
        return next(new ErrorResponse('Invalid credentials', 401));
    }

    sendTokenResponse(user, 200, res);
});

// Get token from model, create cookie and send response
const sendTokenResponse = (user, statusCode, res) => {
    // Create token
    const token = user.getSignedJwtToken();

    const options = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000), // 30 days from now
        httpOnly: true, // We only wont to cookie to be access from client side script
    };

    if (process.env.NODE_ENV === 'production') {
        options.secure = true // work with https
    }

    res
        .status(statusCode)
        .cookie('token', token, options)
        .json({ 
            success: true,
            token
        });
}

/**
 * @description Get current logged in user
 * @route       GET /api/auth/loggedUser
 * @access      Private
 */

const loggedUser = asyncHandler(async(req, res, next) => {
    const user = await User.findById(req.user.id);

    res.status(200).json({ success: true, data: user })
});


module.exports = {
    register,
    login,
    loggedUser
}

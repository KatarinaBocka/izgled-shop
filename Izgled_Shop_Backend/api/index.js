const Router = require('express').Router();

Router.use('/', require('./auth'));
Router.use('/', require('./products'));

module.exports = Router;
const Router = require('express').Router();
const controller = require('./products.controller');
const { authProtectMiddleware, authorize} = require('../../middleware/auth');

Router.get('/products', authProtectMiddleware, authorize('admin', 'user'), controller.getAllProducts);
Router.get('/products/:id', controller.getProduct);
Router.post('/products', controller.createProduct);
Router.put('/products/:id', controller.updateProduct);
Router.delete('/products/:id', controller.deleteProduct);

module.exports = Router;
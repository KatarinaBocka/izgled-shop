
const getAllProducts = () => {
        const products = [
            { 
                id: '1',
                name: 'Kuhinja',
                type: 'stolarski'
            },
            { 
                id: '2',
                name: 'Kauc',
                type: 'Mebl'
            },
            { 
                id: '3',
                name: 'Stolica',
                type: 'stolovi i stolice'
            },
        ];
        
        return products; 
}

module.exports = {
    getAllProducts
}
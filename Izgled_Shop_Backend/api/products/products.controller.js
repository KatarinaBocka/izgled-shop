const Product = require('../../database/models/Products');

const service = require('./products.services');

// @desc        Get all products
// @route       GET /api/products
// @access      Public
const getAllProducts =  (req, res, next) => {
    //await res.status(200).json({success: true});

    //const products = service.getAllProducts();
    res.status(200).json({success: true, hello: "test " });
};

// @desc        Get single product
// @route       GET /api/products/:id
// @access      Public
const getProduct =  (req, res) => {
   res.status(200).json({success: true});
};

// @desc        Create new product
// @route       POST /api/products
// @access      Private
const createProduct = async (req, res) => {
    try {
        const product = await Product.create(req.body);
        res.status(201).json({success: true, data: product})
    } catch (err) {
        res.status(400).json({success: false});
    }
    
};

// @desc        Update product
// @route       PUT /api/products/:id
// @access      Private
const updateProduct =  (req, res) => {
    res.status(200).json({success: true});
}

// @desc        Delete product
// @route       DELETE /api/products/:id
// @access      Private
const deleteProduct = (req, res) => {
    res.status(200).json({success: true});
};

module.exports = {
    getAllProducts,
    getProduct,
    createProduct,
    updateProduct,
    deleteProduct
}
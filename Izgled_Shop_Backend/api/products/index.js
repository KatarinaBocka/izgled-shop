const Router = require('express').Router();

Router.use('/', require('./products.router'));

module.exports = Router;
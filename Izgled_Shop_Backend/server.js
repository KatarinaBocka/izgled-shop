const express = require('express');
const dotenv = require('dotenv');
const logger = require('./config/logger');
const colors = require('colors');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const connectDB = require('./config/db');

// Load env vars
dotenv.config({ path: './config/config.env' });

// Connset to database
connectDB();

// Route files
const api = require('./api');

const app = express();

// Body parser
app.use(express.json());

// Cookie parser
app.use(cookieParser());

// Dev logging middleware
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// Mount routers
app.use('/api', api);

const PORT = process.env.PORT || 8082;

const server = app.listen(
    PORT, 
    console.log(`Server running on port ${PORT}!`.yellow.bold)
);

// Handle unhandled rejections
process.on('unhandledRejection', (err, promise) => {
    logger.error(`Error: ${err.message}`);
    // Close server & exit process
    server.close(() => process.exit(1));
});
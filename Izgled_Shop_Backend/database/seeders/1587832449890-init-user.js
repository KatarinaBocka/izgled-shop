const mongoose = require('mongoose'); // We will have separate connection for seeder
const colors = require('colors'); // For console error, info.. color
const dotenv = require('dotenv'); // because we need access to mongo URI

// Load env vars
dotenv.config({ path: '../../config/config.env' });

// Load models
const User = require('../models/User');

// Connect to DB
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
});

// Read JSON files
const users = [
    {
        "name": "Katarina",
        "email": "katarina.bocka@gmail.com",
        "role": "user",
        "password": "Passw0rd"
    }
]

// Import into DB
const importData = async () => {
    try {
        await User.create(users)
        console.log('Data imported successfully.'.green.inverse);
        process.exit();
    } catch (err) {
        console.error(err);
    }
}

// Remove  DB
const deleteData = async () => {
    try {
        await User.deleteMany()
        console.log('Data destroyed successfully.'.red.inverse);
        process.exit();
    } catch (err) {
        console.error(err);
    }
}

// Import data: cd Izgled_Shop_Backend\database\seeders node 1587832449890-init-user -i
if (process.argv[2] === '-i') {
    importData();
} else if (process.argv[2] === '-d') {
    deleteData();
}
const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please add a name.'],
        unique: true,
        trim: true,
        maxlength: [50, 'Name can not be more than 50 characters.']
    },
    slug: String,
    description: {
        type: String,
        required: [true, 'Please add a description.'],
        maxlength: [500, 'Description can not be more than 50 characters.']
    },
    category: {
        type: String,
        required: true,
        enum: [
            'Stolarski proizvodi',
            'Mebl proizvodi',
            'Stolovi i stolice',
            'Unikati'
        ]
    },
    type: {
        type: String,
        required: true,
        enum: [
            'Kuhinja',
            'Plakar',
            'Komoda',
            'Sto',
            'Stolica',
            "Ugaona garnitura",
            'Kauc',
            'Lezaj',
            'Fotelja',
            'TDF'
        ]
    },
    photo: {
        type: String,
        //required: true
    },
    price: Number,
    available: {
        type: Boolean,
        default: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Product', ProductSchema);

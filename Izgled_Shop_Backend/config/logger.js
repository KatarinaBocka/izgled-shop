const { createLogger, format, transports } = require('winston');
require('winston-rsyslog').Rsyslog;
require('winston-daily-rotate-file');

const logFormat = format.combine(
    format.colorize(),
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    format.simple(),
    format.printf(info => `${info.timestamp} [nerve-ovdm] ${info.level}: ${info.message}`)
);

// Always log to console
const logger = createLogger({
    format: logFormat
});

if (process.env.NODE_ENV === 'production') {
    /* Production environment
  
    Facility levels:
    16 : local use 0 (local0)
    17 : local use 1 (local1)
    18 : local use 2 (local2)
    19 : local use 3 (local3)
    20 : local use 4 (local4)
    21 : local use 5 (local5)
    22 : local use 6 (local6)
    23 : local use 7 (local7)
    */
    logger.add(new transports.Rsyslog({ protocol: 'U', tag: 'ovdm', facility: 23, level: 'info' }));
  
    // File log
    logger.add(
      new transports.DailyRotateFile({
        filename: '/var/log/nerve-ovdm/nerve-ovdm.log',
        datePattern: 'YYYY-MM-DD',
        level: 'info',
        maxFiles: 7,
        auditFile: 'nerve-ovdm.audit.json'
      })
    );
}

module.exports = logger;